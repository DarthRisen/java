import java.util.Scanner;
public class While3
{
	public static void main(String[] args)
	{
		Scanner sc= new Scanner(System.in);
		int a=sc.nextInt();
		int i=1,j=1;
		while(true)
		{
			if(i>a)
			{
				break;
			}
			j=j*i;
			i++;
		}
		System.out.println("fact = "+j);
		sc.close();
	}
}